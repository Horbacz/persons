package persons;

public abstract class Persons {
    protected String name;
    protected String surname;
    protected int id;

    Persons() {

    }
    void showData(String name, String surname, int id)
    {
        this.name = name;
        this.surname = surname;
        this.id = id;
        System.out.println("Fullname = " + name + " " + surname + " id: " + id + " vocation: " + getVocation());
    }

    protected abstract String getVocation();

    protected abstract String sayHello();

}