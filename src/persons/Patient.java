package persons;

 class Patient extends Persons {

     @Override
     protected String getVocation() {
         return "Patient";
     }

     @Override
     protected String sayHello() {
         return "Hello, I'm a patient";
     }
 }
