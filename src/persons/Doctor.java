package persons;

class Doctor extends Persons {

    @Override
    protected String getVocation() {
        return "Doctor";
    }

    @Override
    protected String sayHello() {
        return "Hello, I'm a doctor";
    }
}

