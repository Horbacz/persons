package persons;

public class Main {

    public static void main(String[] args) {
        Doctor alfred = new Doctor();
        alfred.showData("Alfred", "Barka", 12345);
        Patient martin = new Patient();
        martin.showData("Martin", "Ser", 5678);

        talkToMe(alfred);
        talkToMe(martin);
    }

    static void talkToMe(Persons personData) {
        System.out.println(personData.sayHello());
    }
}
